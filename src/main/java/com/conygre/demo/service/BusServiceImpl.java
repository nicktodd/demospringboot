package com.conygre.demo.service;

import com.conygre.demo.entities.Bus;
import com.conygre.demo.repo.BusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
@Service
public class BusServiceImpl implements BusService {

    @Autowired
    private BusRepository busRepository;


    @Override
    public Collection<Bus> getAllBuses() {
        return busRepository.findAll();
    }

    @Override
    public Bus getBusById(int id) {
        return busRepository.findById(id).get();
    }

    @Override
    public Bus updateBus(Bus updatedBus) {
        return busRepository.save(updatedBus);
    }

}
