package com.conygre.demo.service;

import com.conygre.demo.entities.Bus;

import java.util.Collection;

public interface BusService {
    Collection<Bus> getAllBuses();
    Bus getBusById(int id);

    Bus updateBus(Bus updatedBus);
}
