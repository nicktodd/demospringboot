CREATE TABLE `conygre`.`buses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  `colour` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

  INSERT INTO `conygre`.`buses` (`id`, `type`, `colour`) VALUES ('1', 'Routemaster', 'red');
  INSERT INTO `conygre`.`buses` (`id`, `type`, `colour`) VALUES ('2', 'RT', 'green');
  INSERT INTO `conygre`.`buses` (`id`, `type`, `colour`) VALUES ('3', 'Bedford OB', 'brown');

