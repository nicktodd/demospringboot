package com.conygre.demo.rest;

import com.conygre.demo.entities.Bus;
import com.conygre.demo.service.BusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
@RestController
@RequestMapping("/buses")
@CrossOrigin // only needed if called by JS from a browser
public class BusController {

    @Autowired
    private BusService busService;

    @GetMapping
    //@RequestMapping(method= RequestMethod.GET)
    public Collection<Bus> getBuses() {
        return busService.getAllBuses();
    }

    @GetMapping("/{busId}")
    public Bus getBusById(@PathVariable("busId") int id) {
        return busService.getBusById(id);
    }

    @PutMapping
    public Bus updateBus(@RequestBody Bus updatedBus) {
        return busService.updateBus(updatedBus);
    }



}
